# Data mining and topical classification
- extract_db.py - retrieved the raw data based on our query from postgres database running on our server. The Postgres query we used was SELECT raw_data from newsfeed WHERE raw_data is not null and create_time LIKE '2012-{mon:02d}-{num:02d}%'  and outputs the file that contains the filtered data based on year, month and date.
- topic_classify.py - Classify topics based on the User Id and Post_description

Packages used:

sys

psycopg2

json

re

subprocess

csv

locale

# References

https://nlp.stanford.edu