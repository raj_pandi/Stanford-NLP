"""Author: Rajkumar Pandi"""
""" This code is specifically used to extract the necessary data files from the database"""
import sys
import psycopg2
import json
import re
import subprocess

def main():
    try:
        conn=psycopg2.connect("dbname='raj1pagekeep'") /*change the database name*/
    except:
        print "Database Connection Failed"
    cur = conn.cursor()
    for month in xrange(1,13):
        text = open("UserCountMonth"+str(month),"a")
        subprocess.call(['chmod','0666',"UserCountMonth"+str(month)])
        for date in xrange(1,32):
            cur.execute("""SELECT raw_data from newsfeed WHERE raw_data is not null and create_time LIKE '2012-{mon:02d}-{num:02d}%';""".format(num=date,mon=month))
            rows=cur.fetchall()
            text_file = open("2012"+"-"+str(month)+"-"+str(date),"w")
            subprocess.call(['chmod','0666',"2012"+"-"+str(month)+"-"+str(date)])
            count_from = []
            for i in range(len(rows)):
                g = rows[i][0]
                n = json.dumps(g)
                try:
                    h =json.loads(g)
                    count_from.append(str(h[u'from']))
                    del h[u'from']
                    k = str(h[u'caption'])+str(',')+str(h[u'comment'])+str(',')+str(h[u'description'])+str(',')+str(h[u'name'])
                    text_file.write(k)
                except:
                    e= sys.exc_info()[0]
            c_from = len(set(count_from))
            text.write("2012"+"-"+str(month)+"-"+str(date)+"=>"+str(c_from))
            text.write("\n")
            print '-------------------'
            print "Total User:", c_from
            print '-------------------'
            text_file.close()
        text.close()

if __name__ == '__main__':
    main()
