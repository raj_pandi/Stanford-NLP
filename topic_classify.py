"""Author: Rajkumar Pandi"""
#!/usr/bin/python
import csv
import locale
import numpy as np
import sys
"""This code choose the topic number that has maximum probability"""
"""This code takes 2 arguments input file and output file"""
def post_insert():
    p=[]
    with open('2012_politics.csv') as t:
        reader = csv.DictReader(t)
        for row in reader:
            data = row['POST_DESCRIPTION']
            p.append([data])
    return p

def main():
    if (len(sys.argv)) != 3:
        return 0
    p = post_insert()
    txt_file = open(sys.argv[2],'w')
    k = [['USER_INFO','POST_DESCRIPTION','TOPIC_NO']]
    an = csv.writer(txt_file)
    an.writerows(k)
    f = open(sys.argv[1],'r')
    i = 0
    for line in f:
        if ("USER_INFO" in line):
            continue
        nline = line[line.rfind("}") + 3:].split(",")
        user_info = line[:line.rfind("}")+2]
        #z = ','.join(nline)
        #print z
        del(nline[0])
        z = map(float, nline)
        z_max = (max(z))
        g = [[user_info,p[i],str(z.index(z_max))]]
        an.writerows(g)
        i=i+1
    txt_file.close()

if __name__ == "__main__":
    main()
